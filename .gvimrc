set ts=4 " tabstop - how many columns should the cursor move for one tab
set sw=4 " shiftwidth - how many columns should the text be indented
set wrap " wraps longs lines to screen size
colorscheme desert 
set ai " always set autoindenting on
set number
set showmatch " show matching brackets
syntax on " Switch on syntax highlighting.
set hlsearch " Switch on search pattern highlighting.
set mousehide " Hide the mouse pointer while typing.
set history=80
set hlsearch " highlights the previously searched string
set incsearch " higlight search string as search pattern is entered
